import DS from 'ember-data';

export default DS.Model.extend({ 
    title: DS.attr(),
    lat: DS.attr(),
    lng: DS.attr(),
    rate: DS.attr(),
    address: DS.attr(),
    district: DS.attr(),
    bathrooms: DS.attr(),
    bedrooms: DS.attr(),
    images: DS.attr(),
    price: DS.attr(),
    isfeatured: DS.attr(),
    propmeta: DS.attr(),
    retinaIcon: DS.attr(),
    thumbnail: DS.attr(),
    url: DS.attr(),
    description: DS.attr(),
    refrigerator: DS.attr(),
    tv: DS.attr(),
    propertySize: DS.attr(),
    name: DS.attr(),
    lawn: DS.attr(),
    washer: DS.attr(),
    sauna: DS.attr(),
    city: DS.attr(),
    laundry: DS.attr(),
    airConditioning: DS.attr(),
    wifi: DS.attr(),
    wechat: DS.attr(),
    windowCoverings: DS.attr(),
    amenities: DS.attr(),
    gym: DS.attr(),
    equipment: DS.attr(),
    dryer: DS.attr(),
    garage: DS.attr(),
    phone: DS.attr(),
    poolSize: DS.attr(),
    additionalRooms: DS.attr(),
    swimmingPool: DS.attr(),
    microwave: DS.attr(),
    outdoorShower: DS.attr(),
    yearBuilt: DS.attr(),
    barbeque: DS.attr(),
    suburb: DS.attr()
});
