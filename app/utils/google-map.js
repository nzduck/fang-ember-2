import Ember from 'ember';

export default Ember.Object.extend({

  createMap(center) {
    let houzezMapOptions = {
      zoom: 12,
      maxZoom: 12,
      center: center,
      disableDefaultUI: true,
      scrollwheel: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scroll: { x: $(window).scrollLeft(), y: $(window).scrollTop() }
    };
    return new google.maps.Map(document.getElementById("map"), houzezMapOptions);
  }

});