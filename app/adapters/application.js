import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
    host: 'https://s4xqv4qi3m.execute-api.ap-southeast-2.amazonaws.com',
    namespace: 'dev'
});
