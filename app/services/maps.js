import Ember from 'ember';
import mapUtil from '../utils/google-map';

export default Ember.Service.extend({

    init() {
        if (!this.get('cachedMap')) {
            this.set('cachedMap', Ember.Object.create());
        }
        if (!this.get('mapUtil')) {
            this.set('mapUtil', mapUtil.create());
        }
    },
    renderMap(datamodel) {
        let bounds = this.getBounds(datamodel);
        let theMap = this.getMap(datamodel, bounds.getCenter());
        // var markers = new Array();
        datamodel.forEach((item, i) => {
            let marker = this.getMarker(item, theMap);
            // markers.push(marker);
            let infobox = this.getInfoBox(item);
            this.attachInfoBoxToMarker(theMap, marker, infobox);
        });
        theMap.fitBounds(bounds);
    },
    getBounds(datamodel) {
        var bounds = new google.maps.LatLngBounds();
        datamodel.forEach(function (item) {
            console.log("lat:" + item.lat + "lng:" + item.lng);
            bounds.extend(new google.maps.LatLng(item.lat, item.lng));
        });
        return bounds;
    },
    getMap(datamodel, center) {
        //let map = this.get(`cachedMap.map`);
       // if (!map) {
          let  map = this.get('mapUtil').createMap(center);
            //this.set(`cachedMap.map`, map);
       // }
        google.maps.event.addListenerOnce(map, 'tilesloaded', function () {
            $('.mapPlaceholder').hide();
        });

        var offset = $(map.getDiv()).offset();
        //map.panBy(((houzezMapOptions.scroll.x - offset.left) / 3), ((houzezMapOptions.scroll.y - offset.top) / 3));
        google.maps.event.addDomListener(window, 'scroll', function () {
            var scrollY = $(window).scrollTop(),
                scrollX = $(window).scrollLeft(),
                scroll = map.get('scroll');
            if (scroll) {
                map.panBy(-((scroll.x - scrollX) / 3), -((scroll.y - scrollY) / 3));
            }
            map.set('scroll', { x: scrollX, y: scrollY });
        });

        return map;
    },

    getMarker(item, theMap) {
        // var marker_url = item.get('data').icon;
        //console.log(item.get('data').icon);
        var marker_url = "images/map/pin-apartments.png";
        var marker_size = new google.maps.Size(44, 56);
        if (window.devicePixelRatio > 1.5) {
            if (item.retinaIcon) {
                marker_url = item.retinaIcon;
                marker_size = new google.maps.Size(84, 106);
            }
        }

        var marker_icon = {
            url: marker_url,
            size: marker_size,
            scaledSize: new google.maps.Size(44, 56),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(7, 27)
        };

        return new google.maps.Marker({
            map: theMap,
            draggable: false,
            position: new google.maps.LatLng(item.lat, item.lng),
            icon: marker_icon,
            title: item.title,
            animation: google.maps.Animation.DROP,
            visible: true
        });
    },

    getInfoBox(item) {
        var infoBoxText = document.createElement("div");
        infoBoxText.className = 'property-item item-grid map-info-box';
        infoBoxText.innerHTML =
            '<div class="figure-block">' +
            '<figure class="item-thumb">' +
            item.isfeatured +
            '<div class="price hide-on-list">' +
            item.price +
            '</div>' +
            '<a href="' + item.url + '" tabindex="0">' +
            item.thumbnail +
            '</a>' +
            '<div class="thumb-caption cap-actions clearfix">' +
            '<div class="pull-right">' +
            '<span title="" data-placement="top" data-toggle="tooltip" data-original-title="Photos">' +
            '<i class="fa fa-camera"></i> <span class="count">(' + item.images_count + ')</span>' +
            '</span>' +
            '</div>' +
            '</div>' +
            '</figure>' +
            '</div>' +
            '<div class="item-body">' +
            '<div class="body-left">' +
            '<div class="info-row">' +
            '<h2 class="property-title"><a href="' + item.url + '">' + item.title + '</a></h2>' +
            '<h4 class="property-location">' + item.address + '</h4>' +
            '</div>' +
            '<div class="table-list full-width info-row">' +
            '<div class="cell">' +
            '<div class="info-row amenities">' +
            item.prop_meta +
            '<p>' + item.type + '</p>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        // var infoBoxOptions = {
        //     content: infoBoxText,
        //     disableAutoPan: true,
        //     maxWidth: 0,
        //     alignBottom: true,
        //     pixelOffset: new google.maps.Size(-122, -48),
        //     zIndex: null,
        //     closeBoxMargin: "0 0 -16px -16px",
        //     closeBoxURL: "images/map/close.png",
        //     infoBoxClearance: new google.maps.Size(1, 1),
        //     isHidden: false,
        //     pane: "floatPane",
        //     enableEventPropagation: false
        // };
        // let infobox = new InfoBox(infoBoxOptions);
        // return infobox;
    },

    attachInfoBoxToMarker(map, marker, infoBox) {
        marker.addListener('click', function () {
            var scale = Math.pow(2, map.getZoom());
            var offsety = ((100 / scale) || 0);
            var projection = map.getProjection();
            var markerPosition = marker.getPosition();
            var markerScreenPosition = projection.fromLatLngToPoint(markerPosition);
            var pointHalfScreenAbove = new google.maps.Point(markerScreenPosition.x, markerScreenPosition.y - offsety);
            var aboveMarkerLatLng = projection.fromPointToLatLng(pointHalfScreenAbove);
            map.setCenter(aboveMarkerLatLng);
            infoBox.close();
            infoBox.open(map, marker);
        });
    }

   
    

});
