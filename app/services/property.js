import Ember from 'ember';

export default Ember.Service.extend({

  store: Ember.inject.service(),
  

  listProperties() {
    return this.get('store').findAll('rental');
  }
});
