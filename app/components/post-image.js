import Ember from "ember";
const RSVP = Ember.RSVP;
const set = Ember.set;

export default Ember.Component.extend({

  fileQueue: Ember.inject.service(),
  images: Ember.computed(function() {
      return [5,4,3,2,1,0];
      //return Array.apply(null,Array[5]).map((x,i) => i);
      
  }),
  availableImages: Ember.computed(function() {
    return this.get('images').length;
  }),


  actions: {
    uploadImage: function(file) {

      const images = this.get('images');
      if(images.length < file.queue.files.length){
        console.log("error=================");
        return;
      }

      console.log(file);
      //let model = this.modelFor(this.routeName);
      var random = Math.random().toString(36).substr(2, 9);
      var apiBaseURL = "https://dsrgwusw9l.execute-api.ap-southeast-2.amazonaws.com/dev";   
      Ember.$.ajax({
        url: apiBaseURL+"/requestUploadURL",
        type: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({
          name: random + ".jpg",
          type: "image/jpeg"
        })
      }).then((response) => {
        return file.uploadBinary(response.uploadURL, {           
            headers: {
              'Content-Type':"image/jpeg"    
            },
            method: "PUT",            
            data: new Blob([file.blob], {type: "image/jpeg"})         
          });
      }).then((response) => {
          
          var  index = images.pop();
          console.log("uploaded:" + index + images.length);
          var urlimage = 'https://s3-ap-southeast-2.amazonaws.com/familys.images/'+ random + '.jpg' ;
          Ember.$('#preview-image'+ index)[0].src = urlimage; 
          this.set('availableImages',images.length);
                                 
      }).catch(function(error){
        console.log(error);
        // handle errors here
      });

    }
  }
});
