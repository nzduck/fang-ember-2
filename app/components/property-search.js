import Ember from "ember";

export default Ember.Component.extend({
    regions: [{ label: "All New Zealand", value: "0" },
    { label: "Auckland", value: "1" },{ label: "Bay of Plenty", value: "2" }],
  
    districts: [{label: "All Districts", value:"0"}],
    districtDisabled: true,
    suburbs: [{label:"All Suburbs", value:"0"}],
    suburbDisabled: true,
    propertyTypes: [{label: "All Property Types",value :"0"},{label: "Apartment",value :"1"},{label: "Houses",value :"2"},{label: "Unit",value :"3"},{label: "Other",value :"4"}],
    prices: [{label: "Price",value :"0"},{label: "$50 - $100 /week",value :"1"},{label: "$100 - $200 /week",value :"2"},
    {label: "$200 - $300 /week",value :"3"},{label: "$300 - $400 /week",value :"4"},{label: "$500 - $600 /week",value :"5"},{label: "$600 - $700 /week",value :"6"},{label: "$700 - $800 /week",value :"7"},{label: "$$800+ /week",value :"8"}],
    bedrooms: [{label: "Bedrooms",value :"0"},{label: "1",value :"1"},{label: "2",value :"2"},{label: "3",value :"3"},{label: "4+",value :"4"},],
    bathrooms: [{label: "Bathrooms",value :"0"},{label: "1",value :"1"},{label: "2",value :"2"},{label: "3",value :"3"},{label: "4+",value :"4"},],
    parkings: [{label: "Parking",value :"0"},{label: "1",value :"1"},{label: "2",value :"2"},{label: "3",value :"3"},{label: "4+",value :"4"},],

    actions: {
      change: function(e) {
        if(e.id === Ember.$('#region')[0].id) {
          
              if(Ember.$('#region')[0].value !==0) {
                            this.set('districtDisabled', false);
                            // let filterAction = this.get('filterByArea');
                            //              filterAction('b').then((filterResults) => {
                            //                if (filterResults.query === 'b') {
                            //                  this.set('results', filterResults.results)
                            //                }
                            //               });

              if(Ember.$('#region')[0].value == 1) {
                                let dis = [{label: "All Districts", value:"0"},{ label: "Auckland City", value: "1" },
                                { label: "Waitakere City", value: "2" },{ label: "Franklin", value: "3" },{ label: "Hauraki Gulf Islands", value: "4" },
                                { label: "Manukau City", value: "5" },{ label: "North Shore City", value: "6" },{ label: "Papakura", value: "7" },
                                { label: "Rodney", value: "8" },{ label: "Waiheke Island", value: "9" }]
                                this.set('districts',dis);
                                this.get('filterByCity')('a');
                         
              }
              if(Ember.$('#region')[0].value == 2) {
                              let dis = [{label: "All Districts", value:"0"},{ label: "Tauranga", value: "1" },
                              { label: "Rotorua", value: "2" }]
                              this.set('districts',dis);
                              this.get('filterByCity')('c');
              }
            }
          }
          if(Ember.$('#region')[0].value ==0){
            this.set('districtDisabled', true);
            this.set('suburbDisabled' ,true);
            this.set('districts', [{label: "All Districts"}]);
            this.set('suburbs', [{label: "All Districts"}]);
            this.get('filterByCity')('');
          }

          if(e.id=== Ember.$('#district')[0].id){
            if(Ember.$('#district')[0].value !==0) {
              this.set('suburbDisabled', false);

            if(Ember.$('#district')[0].value == 1 && Ember.$('#region')[0].value == 1 ) {
                  let dis = [{label: "All Suburbs", value:"0"},{ label: " City Centre ", value: "1" },
                  { label: "Hillsborough", value: "2" }]
                  this.set('suburbs',dis);
                  this.get('filterByDistrict')('aa');
             }
            if(Ember.$('#district')[0].value == 2 && Ember.$('#region')[0].value == 1) {
                let dis = [{label: "All Suburbs", value:"0"},{ label: "Henderson", value: "1" },
                { label: "Piha", value: "2" }]
                this.set('suburbs',dis);
                this.get('filterByDistrict')('ab');
            }
            if(Ember.$('#district')[0].value == 1 && Ember.$('#region')[0].value == 2 ) {
              let dis = [{label: "All Suburbs", value:"0"},{ label: " Bayfair ", value: "1" },
              { label: "Mount Maunganui ", value: "2" }]
              this.set('suburbs',dis);
              this.get('filterByDistrict')('ca');
            }
            if(Ember.$('#district')[0].value == 2 && Ember.$('#region')[0].value == 2) {
              let dis = [{label: "All Suburbs", value:"0"},{ label: "Atiamuri", value: "1" },
              { label: "Awahou", value: "2" }]
              this.set('suburbs',dis);
              this.get('filterByDistrict')('cb');
            }
          }
          }
          if(Ember.$('#district')[0].value ==0){
            this.set('suburbDisabled' ,true);
            this.set('suburbs', [{label: "All Districts"}]);
            if(Ember.$('#region')[0].value == 1) {
              this.get('filterByCity')('a');
            }else if(Ember.$('#region')[0].value == 2) {
              this.get('filterByCity')('c');
            }
          }
      }
    }

   
    
});
