import Ember from 'ember';

const google = window.google;

export default Ember.Component.extend({

    // property: Ember.inject.service(),
    store: Ember.inject.service(),
    maps: Ember.inject.service(),


    didInsertElement() {        
        this._super(...arguments);       
        var datamodel =  this.get('datamodel');
        this.get('maps').renderMap(datamodel);
    },
    didUpdate() {        
        this.get('datamodel').then((dataList) => {
            this.get('maps').renderMap(dataList);
          });

    }
});
