import Ember from 'ember';

export default Ember.Route.extend({
    property: Ember.inject.service(),
    model(param) {
        return this.get('store').findRecord('rental', param.id);       
    },

    actions: {
        scrollTop(){
            window.scrollTo(0, 0);
        }

    }     
});
