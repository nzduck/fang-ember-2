import Route from '@ember/routing/route';
import fetch from 'ember-fetch/ajax';

export default Route.extend({
    model() {
        return fetch('https://s4xqv4qi3m.execute-api.ap-southeast-2.amazonaws.com/dev/rentals')
          .then(function(response) {
            return response;
          });
      }
});
