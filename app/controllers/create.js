import Ember from "ember";
import DS from "ember-data";

export default Ember.Controller.extend({
  errors: DS.Errors.create(),
  actions: {
    createProperty(model) {
      if (this.validate()) {
        var model = this.get("model");
        var houseImages = document.getElementsByName("houseImage");
        var images = [...houseImages]          
          .filter(image => image.src != "http://placehold.it/127x127")
          .map(house => {
            return {src:house.src}
          });
        this.set("model.images", images);       
        var rental = this.store.createRecord("rental", model);        
        rental
          .save()
          .then(() => this.transitionToRoute("view", rental.id))
          .catch(() => this.transitionToRoute("home"));
      }
    }
  },

  validate() {
    this.set("errors", DS.Errors.create());

    if (
      this.get("model.title") === "" ||
      this.get("model.title") === undefined
    ) {
      this.get("errors").add("title", "title can't be empty");
    }
    if (
      this.get("model.description") === "" ||
      this.get("model.description") === undefined
    ) {
      this.get("errors").add("description", "description can't be empty");
    }
    if (
      this.get("model.address") === "" ||
      this.get("model.address") === undefined
    ) {
      this.get("errors").add("address", "address can't be empty");
    }
    if (
      this.get("model.district") === "" ||
      this.get("model.district") === undefined
    ) {
      this.get("errors").add("district", "district can't be empty");
    }
    if (
      this.get("model.neighborhood") === "" ||
      this.get("model.neighborhood") === undefined
    ) {
      this.get("errors").add("neighborhood", "neighborhood can't be empty");
    }
    if (this.get("model.city") === "" || this.get("model.city") === undefined) {
      this.get("errors").add("city", "city can't be empty");
    }
    if (
      isNaN(this.get("model.price")) ||
      this.get("model.price") < 0 ||
      String(this.get("model.price")).indexOf(".") > -1
    ) {
      this.get("errors").add("price", "price must be a positive number");
    }
    if (
      isNaN(this.get("model.bedrooms")) ||
      this.get("model.bedrooms") < 0 ||
      String(this.get("model.bedrooms")).indexOf(".") > -1
    ) {
      this.get("errors").add("bedrooms", "bedrooms must be a positive number");
    }
    if (
      isNaN(this.get("model.bathrooms")) ||
      this.get("model.bathrooms") < 0 ||
      String(this.get("model.bathrooms")).indexOf(".") > -1
    ) {
      this.get("errors").add(
        "bathrooms",
        "bathrooms must be a positive number"
      );
    }
    if (
      isNaN(this.get("model.garage")) ||
      this.get("model.garage") < 0 ||
      String(this.get("model.garage")).indexOf(".") > -1
    ) {
      this.get("errors").add("garage", "garage must be a positive number");
    }
    if (
      isNaN(this.get("model.phone")) ||
      this.get("model.phone") < 0 ||
      String(this.get("model.phone")).indexOf(".") > -1
    ) {
      this.get("errors").add("phone", "this is not a phone number");
    }
    return this.get("errors.isEmpty");
  }
});
